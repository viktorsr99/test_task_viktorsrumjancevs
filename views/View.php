<?php

class View {

    public function render ($view, $data = []) {
        include '../views/'.$view.'.php';
    }

}
