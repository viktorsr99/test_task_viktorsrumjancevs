<?php include_once 'includes/header.php' ?>
<form action="index.php?page=product_add_submitted" id="addProductForm" method="post">
    <div class="clearfix">
        <h3 class="float-left">Add product<button type="button" id="productList" class="btn btn-link btn-sm pt-0">List</button></h3>
        <button type="button" id="submitButton" class="btn btn-outline-secondary float-right">Save</button>
    </div>
    <hr>
    <div class="alert alert-danger" id="fillAllFieldsAlert" role="alert">
        Please complete all fields
    </div>
    <div class="col-md-6 mt-5">
        <div class="form-group row">
            <label for="sku" class="col-md-3 col-form-label">SKU</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="sku" name="sku">
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-md-3 col-form-label">Name</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="name" name="name">
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-md-3 col-form-label">Price</label>
            <div class="col-md-9">
                <input type="text" class="form-control" id="price" name="price">
            </div>
        </div>
        <div class="form-group row">
            <label for="type" class="col-md-3 col-form-label">Type switcher</label>
            <div class="col-md-9">
                <select class="form-control" id="type" name="type">
                    <option value="1">DVD-disc</option>
                    <option value="2">Book</option>
                    <option value="3">Furniture</option>
                </select>
            </div>
        </div>
        <div class="type mt-5">
            <div id="type-furniture">
                <div class="form-group row">
                    <label for="height" class="col-md-3 col-form-label">Height</label>
                    <div class="col-md-9">
                        <input type="number" class="form-control" id="height" name="height">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="width" class="col-md-3 col-form-label">Width</label>
                    <div class="col-md-9">
                        <input type="number" class="form-control" id="width" name="width">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="length" class="col-md-3 col-form-label">Length</label>
                    <div class="col-md-9">
                        <input type="number" class="form-control" id="length" name="length">
                    </div>
                </div>
                <div class="alert alert-light" role="alert">
                    Please provide dimensions in HxWxL format
                </div>
            </div>
            <div id="type-dvd">
                <div class="form-group row">
                    <label for="name" class="col-md-3 col-form-label">Size</label>
                    <div class="col-md-9">
                        <input type="number" class="form-control" id="size" name="size">
                    </div>
                </div>
                <div class="alert alert-light" role="alert">
                    Please provide size in MB
                </div>
            </div>
            <div id="type-book">
                <div class="form-group row">
                    <label for="weight" class="col-md-3 col-form-label">Weight</label>
                    <div class="col-md-9">
                        <input type="number" class="form-control" id="weight" name="weight">
                    </div>
                </div>
                <div class="alert alert-light" role="alert">
                    Please provide weight in KG
                </div>
            </div>
        </div>
    </div>
</form>
<?php include_once 'includes/footer.php' ?>