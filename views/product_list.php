<?php include_once 'includes/header.php' ?>
    <div class="clearfix">
        <h3 class="float-left">Product List<button id="addProduct" class="btn btn-link btn-sm pt-0">Add</button></h3>
        <div class="float-right col-md-4 row">
                <select id="action" class="form-control col mr-2">
                    <option value="1">Mass delete</option>
                    <option value="2">Delete all</option>
                </select>
            <button type="button" id="applyButton" class="btn btn-outline-secondary">Apply</button>
        </div>
    </div>
    <hr>
    <form id="productListForm" action="index.php?page=mass_delete_products" method="post" class="row row-cols-1 row-cols-md-3">
        <?php foreach ($data as $product): ?>
        <div class="col mb-4">
            <div class="card" >
                <div class="card-body">
                    <input type="checkbox" class="form-check-input position-static float-right" name="delete[]" value="<?php echo $product['product_id'] ?>">
                    <h5 class="card-title text-center"><?php echo $product['SKU'] ?></h5>
                    <h5 class="card-title text-center"><?php echo $product['name'] ?></h5>
                    <h5 class="card-title text-center"><?php echo number_format($product['price'], 2) ?> $</h5>
                    <h5 class="card-title text-center">
                        <?php
                        echo $product['type_id'] == 1 ? 'Size: ' : ($product['type_id'] == 2 ? 'Weight: ' : ($product['type_id'] == 3 ? 'Dimensions: ' : false));
                        echo $product['value'];
                        echo $product['type_id'] == 1 ? ' MB' : ($product['type_id'] == 2 ? ' KG' : false);
                        ?>
                    </h5>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </form>

<?php include_once 'includes/footer.php' ?>