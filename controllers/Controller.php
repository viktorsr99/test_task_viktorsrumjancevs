<?php

require '../views/View.php';

class Controller {
    private $view;

    public function __construct () {
        $this->view = new View;
    }

    public function pageNotFound () {
        $this->view->render('404');
    }
}