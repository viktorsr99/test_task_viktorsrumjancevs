<?php

require '../models/Product.php';
require '../views/View.php';

class ProductsController {

    private $view;
    private $product;

    private $types = [
        '1' => 'size',
        '2' => 'weight',
        '3' => 'furniture'
    ];

    public function __construct () {
        $this->view = new View;
        $config = require __DIR__ . '/../config/db.php';
        $this->product = new Product($config);
    }

    //Product list page
    public function index () {
        $products = $this->product->getAll();
        $this->view->render('product_list', $products);
    }

    public function add () {
        $this->view->render('product_add');
    }

    //Product add action
    public function addSubmitted ($request) {
        if ($request['sku'] && $request['name'] && $request['price'] && $request['type']) {
            $request['furniture'] = $request['height'] .'x'. $request['width'] .'x'. $request['length'];
            !$request[$this->types[$request['type']]] && $this->redirectTo('product_add');
            $this->product->add($request);
            $this->redirectTo('product_list');
        } else {
            $this->redirectTo('product_add');
        }
    }

    public function massDelete ($request) {
        $this->product->remove($request['delete']);
        $this->redirectTo('product_list');
    }

    public function deleteAll () {
        $this->product->removeAll();
        $this->redirectTo('product_list');
    }

    private function redirectTo ($location = 'product_list') {
        header('Location: index.php?page='.$location);
    }
}