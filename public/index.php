<?php

if (isset($_GET['page'])) {
    switch ($_GET['page']) {
        case 'product_list':
            $controller_name = 'ProductsController';
            $action = 'index';
            break;

        case 'product_add':
            $controller_name = 'ProductsController';
            $action = 'add';
            break;

        case 'product_add_submitted':
            $controller_name = 'ProductsController';
            $action = 'addSubmitted';
            break;

        case 'mass_delete_products':
            $controller_name = 'ProductsController';
            $action = 'massDelete';
            break;

        case 'delete_all_products':
            $controller_name = 'ProductsController';
            $action = 'deleteAll';
            break;

        default:
            $controller_name = 'Controller';
            $action = 'pageNotFound';
            break;
    }
} else {
    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/public/index.php?page=product_list');
}

require __DIR__ . '/../controllers/' . $controller_name . '.php';
$controller = new $controller_name;
$controller->{$action}($_REQUEST);


