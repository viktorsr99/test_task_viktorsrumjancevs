$( document ).ready(function () {
    let types = {
        '1':'dvd',
        '2':'book',
        '3':'furniture'
    }

    $('#type').val('1')
    $('#type').on('change', function () {
        $('#type-dvd').hide();
        $('#type-furniture').hide();
        $('#type-book').hide();
        $(`#type-${types[this.value]}`).show()
    })

    // allows only numbers to input
    function input (evt, tthis) {
        var self = tthis;
        self.val(self.val().replace(/[^0-9\.]/g, ''));
        if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57))
        {
            return evt.preventDefault();
        }
    }
    $("#price").on("input", function(evt) { input(evt, $(this)); });
    $("#size").on("input", function(evt) { input(evt, $(this)); });
    $("#weight").on("input", function(evt) { input(evt, $(this)); });
    $("#height").on("input", function(evt) { input(evt, $(this)); });
    $("#width").on("input", function(evt) { input(evt, $(this)); });
    $("#length").on("input", function(evt) { input(evt, $(this)); });

    //Add product fields check
    $('#submitButton').on('click', function () {
        $('#fillAllFieldsAlert').hide();
        if ($('#sku').val() && $('#name').val() && $('#price').val() && ($('#type').val() == '1' ? $('#size').val() : ($('#type').val() == '2' ? $('#weight').val() : ($('#type').val() == '3' ? (($('#width').val() && $('#height').val() && $('#length').val()) ? true : false) : false)))) {
            $('#addProductForm').submit()
        } else {
            $('#fillAllFieldsAlert').show();
        }
    })

    //Product list actions
    $('#applyButton').on('click', function () {
        $('#action').val() == 1 && $('#productListForm').submit()
        $('#action').val() == 2 && (location.href = 'index.php?page=delete_all_products')
    })

    $('#addProduct').on('click', function () { location.href = 'index.php?page=product_add' })
    $('#productList').on('click', function () { location.href = 'index.php?page=product_list' })

});
