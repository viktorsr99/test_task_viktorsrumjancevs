<?php

class Product {

    private $connectionParams = [
        'host' => '',
        'port' => '',
        'user' => '',
        'password' => '',
        'dbname' => '',
    ];
    private $db;
    private $types = [
        '1' => 'size',
        '2' => 'weight',
        '3' => 'furniture'
    ];

    public function __construct ($config = null) {
        $this->connectionParams = $config['connection']['params'];
        $this->db = new PDO('mysql:host='.$this->connectionParams['host'].';dbname='.$this->connectionParams['dbname'], $this->connectionParams['user'], $this->connectionParams['password']);
    }

    public function getAll () {
        return $this->db->query('SELECT * FROM products INNER JOIN attributes ON products.id = attributes.product_id ORDER BY products.id desc')->fetchAll();
    }

    public function add ($request) {
        $stmt = $this->db->prepare('INSERT INTO products (`SKU`, `name`, `price`) VALUES (:sku, :name, :price)');
        $stmt->bindValue(':sku', $request['sku']);
        $stmt->bindValue(':name', $request['name']);
        $stmt->bindValue(':price', $request['price']);
        $stmt->execute();
        $productId = $this->db->lastInsertId();
        $stmt = $this->db->prepare('INSERT INTO attributes (`type_id`, `value`, `product_id`) VALUES (:type_id, :value, :product_id)');
        $stmt->bindValue(':type_id', $request['type']);
        $stmt->bindValue(':value', $request[$this->types[$request['type']]]);
        $stmt->bindValue(':product_id', $productId);
        $stmt->execute();
        return;
    }

    public function remove ($ids = []) {
        foreach ($ids as $id) {
            $stmt = $this->db->prepare('DELETE FROM products WHERE id = :id');
            $stmt->execute([':id' => $id]);
            $stmt = $this->db->prepare('DELETE FROM attributes WHERE product_id = :id');
            $stmt->execute([':id' => $id]);
        }
    }

    public function removeAll () {
        $stmt = $this->db->prepare('DELETE FROM products');
        $stmt->execute();
        $stmt = $this->db->prepare('DELETE FROM attributes');
        $stmt->execute();
    }

}